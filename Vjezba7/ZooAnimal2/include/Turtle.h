#ifndef TURTLE_H
#define TURTLE_H
#include "Reptile.h"

class Turtle : public Reptile {
protected:
	int dnevni_obrok;
public:
	Turtle(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};

#endif // TURTLE_H
