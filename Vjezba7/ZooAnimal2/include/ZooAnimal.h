#ifndef ZOOANIMAL_H
#define ZOOANIMAL_H
#include <string>
using namespace std;

class Mass
{
    public:
        int god;
        double masa;


};

class ZooAnimal
{
    public:
        ZooAnimal(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated);
        ZooAnimal (const ZooAnimal &a);
        virtual ~ZooAnimal();
        void promjena_obroka (int stanje);
        void unos_mase (int god, double masa);
        int promjena_mase();
        void ispis();

    protected:
        string vrsta;
        string ime;
        int god_rod;
        int kavez;
        int br_obroka;
        int estimated;
        Mass* podaci_masa;


};

#endif // ZOOANIMAL_H
