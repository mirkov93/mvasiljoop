#ifndef TIGER_H
#define TIGER_H
#include "Mammal.h"

class Tiger : public Mammal {
protected:
	int dnevni_obrok;
public:
	Tiger(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje,int dnevni_obrok);

};

#endif // TIGER_H
