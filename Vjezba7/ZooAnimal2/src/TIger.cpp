#include "Tiger.h"

using namespace std;

Tiger::Tiger(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje, int dnevni_obrok) : Mammal(vrsta, ime, god_rod, kavez, br_obroka, estimated,
		gestperiod, avgtemp, razmnozavanje) {
	this->dnevni_obrok = dnevni_obrok;
}
