#include "Mammal.h"

using namespace std;

Mammal::Mammal(string vrsta, string ime, int god_rod, int kavez, int br_obroka, int estimated,
	int gestperiod, double avgtemp, string razmnozavanje) : ZooAnimal(vrsta, ime, god_rod, kavez, br_obroka, estimated) {
	this->gestperiod=gestperiod;
	this->avgtemp=avgtemp;
	this->razmnozavanje="RADANJE";
}


istream& operator>>(istream& is, Mammal& m) {
	cout << "Unesite gestacijski period: ";
	is >> m.gestperiod;
	cout << "Unesite prosjeènu temperaturu: ";
	is >> m.avgtemp;
	return is;
}

ostream& operator<<(ostream& os, Mammal& m) {
	os << "Vrsta: " << m.vrsta << "\nIme: " << m.ime << "\nGodina rodenja: " << m.god_rod <<
		"\nBroj Kaveza: " << m.kavez<< "\nBroj dnevnih obroka: " << m.br_obroka <<
		"\nOcekivani zivotni vijek: " << m.estimated << endl;
	os << "Gestacijski period: " << m.gestperiod<< "\nProsjecna temperatura: " << m.avgtemp << "\nNacin razmnozavanja: " << m.razmnozavanje << endl;
	return os;
}
