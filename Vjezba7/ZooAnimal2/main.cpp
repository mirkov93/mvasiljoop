#include <iostream>
#include <time.h>
#include <vector>
#include "ZooAnimal.h"
#include "Mammal.h"
#include "Bird.h"
#include "Reptile.h"
#include "Crocodile.h"
#include "Elephant.h"
#include "GriffonVulture.h"
#include "Monkey.h"
#include "Owl.h"
#include "Tiger.h"
#include "Turtle.h"

using namespace std;

int main()
{
    Elephant *el = new Elephant("Africki slon", "Dumbo", 1992, 1, 3, 56, 0, 0, "", 2);
	cin >> *el;
	cout << *el;
	Owl *o = new Owl("Usara", "Hedwig", 1989, 2, 6, 28, 0, 0, "", 1);
	cin >> *o;
	cout << *o;
	Crocodile *cro = new Crocodile("Kajman", "Kroki", 2010, 3, 9, 30, 0, 0, "", 5);
	cin >> *cro;
	cout << *cro;

	vector<ZooAnimal*> animals;
	animals.push_back(el);
	animals.push_back(o);
	animals.push_back(cro);

	for (int i = 0; i < animals.size(); i++) {
		cout << animals[i] << endl;
	}
}
