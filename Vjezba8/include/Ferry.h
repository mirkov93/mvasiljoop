#ifndef FERRY_H
#define FERRY_H
#include "watercraft.h"


class Ferry : virtual public watercraft
{
    public:
        Ferry(int p,int b,int a);
        virtual ~Ferry();
        unsigned passengers(){
            return broj_putnika;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // FERRY_H
