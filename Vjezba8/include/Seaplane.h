#ifndef SEAPLANE_H
#define SEAPLANE_H
#include "watercraft.h"
#include "aircraft.h"

class Seaplane : public watercraft, public aircraft
{
    public:
        Seaplane(int n);
        virtual ~Seaplane();
        unsigned passengers(){
            return broj_putnika;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // SEAPLANE_H
