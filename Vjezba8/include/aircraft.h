#ifndef AIRCRAFT_H
#define AIRCRAFT_H
#include "Vehicle.h"
#include <string>
#include <iostream>


class aircraft : virtual public Vehicle
{
    public:
        aircraft();
        virtual ~aircraft();
        std::string type () {
            return "Air" ;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // AIRCRAFT_H
