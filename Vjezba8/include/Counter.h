#ifndef COUNTER_H
#define COUNTER_H
#include "Vehicle.h"
#include "Counter.h"
#include "Bike.h"
#include "Car.h"
#include "Ferry.h"
#include "Catamaran.h"
#include "land_vehicle.h"
#include "watercraft.h"
#include "Seaplane.h"
#include "aircraft.h"

#include <vector>



class Counter
{
    public:
        Counter();
        virtual ~Counter();
        void add (Vehicle* v);
        int total();

    protected:
        int total_putnika;


};

#endif // COUNTER_H
