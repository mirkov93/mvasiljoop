#ifndef WATERCRAFT_H
#define WATERCRAFT_H
#include "Vehicle.h"
#include <string>
#include <iostream>


class watercraft :  public Vehicle
{
    public:
        watercraft();
        virtual ~watercraft();
        std::string type () {
            return "Water" ;
        }

    protected:
        std::string tip_vozila;
        unsigned broj_putnika;

    private:
};

#endif // WATERCRAFT_H
